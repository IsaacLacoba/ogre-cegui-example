CXX=g++ -std=c++11
CXXFLAGS = -ggdb -Wall $(shell pkg-config --cflags OGRE CEGUI-0 OIS) -lCEGUIOgreRenderer-0
LDLIBS = $(shell pkg-config --libs OGRE CEGUI-0 OIS)

main: main.cpp

clean:
	find -name *[~#] -delete
	rm -f config/game.log config/ogre.log main
