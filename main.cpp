// -*- mode:c++ -*-
#include <iostream>
#include <cmath>
#include <string>
#include <vector>
#include <unistd.h>

#include <OgreRoot.h>
#include <OgreLogManager.h>

#include <OgreRenderWindow.h>
#include <OgreWindowEventUtilities.h>
#include <OgreCamera.h>

#include <OgreSceneManager.h>
#include <OgreResourceGroupManager.h>
#include <OgreConfigFile.h>

#include <OgreOverlayContainer.h>
#include <OgreOverlayManager.h>
#include <OgreOverlayElement.h>
#include <OgreTextAreaOverlayElement.h>
#include <OgreFontManager.h>

#include <OgreEntity.h>
#include <OgreVector3.h>
#include <OgreMath.h>
#include <OgreMeshManager.h>

#include <OIS/OISEvents.h>
#include <OIS/OISInputManager.h>
#include <OIS/OISKeyboard.h>
#include <OIS/OISMouse.h>

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>

using namespace std;

#define PLAYER_CAMERA "PlayerCamera"
#define CAMERA_NODE "cameraNode"
#define WINDOW_NAME "ProyectRenderWindow"
#define TARGET_FPS 60.0

enum class EventType{game_event, menu_event};

class InvalidConfig {};

class GameConfig {
  string basedir;

public:
  Ogre::LogManager* logManager;
  Ogre::Log* systemLog;
  Ogre::Log* gameLog;

  string resource;
  string plugins;
  string ogre;
  string ogre_log;
  string game_log;

  GameConfig(string basedir="config/") : basedir(basedir) {
    plugins = basedir + "plugins.cfg";
    ogre = basedir + "ogre.cfg";
    ogre_log = basedir + "ogre.log";
    game_log = basedir + "game.log";
    resource = basedir + "resources.cfg";

    logManager = new Ogre::LogManager();
    systemLog = logManager->createLog(ogre_log, true, false, false);
    gameLog = logManager->createLog(game_log, false, true, false);

  }
};

class Scene {
  Ogre::SceneManager* manager;

  Ogre::SceneNode* get_node_by_name(Ogre::String node_name="") {
    Ogre::SceneNode* node;
    if (node_name.empty())
      node = manager->getRootSceneNode();
    else
      node = manager->getSceneNode(node_name);

    return node;
  }

public:
  Scene(Ogre::Root* root) {
    manager = root->createSceneManager(Ogre::ST_GENERIC);
  }

  Ogre::SceneNode* create_node(Ogre::String name, Ogre::String parent="") {
    Ogre::SceneNode* parent_node = get_node_by_name(parent);

    return parent_node->createChildSceneNode(name);
  }

  Ogre::Entity* create_entity_and_attach(Ogre::String name,
                                         Ogre::String mesh,
                                         Ogre::String target_node="",
                                         bool cast_shadows=true){
    Ogre::SceneNode* node = get_node_by_name(target_node);
    Ogre::Entity* entity = manager->createEntity(name, mesh);

    node->attachObject(entity);
    return entity;
  }

  Ogre::SceneNode* create_node_and_entity(Ogre::String name,
                                          Ogre::String mesh,
                                          Ogre::String parent="") {
    Ogre::SceneNode* node = create_node(name, parent);
    create_entity_and_attach(name, mesh, name);
    return node;
  }

  void create_ground(Ogre::String name="ground",
                     Ogre::String material="Ground",
                     Ogre::String mesh="ground") {
    Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);

    Ogre::MeshManager::getSingleton().createPlane(name,
                                                  Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane,
                                                  200,200,1,1,true,1,20,20,Ogre::Vector3::UNIT_Z)
      ;
    create_node(name);
    Ogre::Entity* groundEntity = create_entity_and_attach(name, mesh, name, false);

    groundEntity->setMaterialName(material);
  }

  Ogre::Camera* create_camera(Ogre::RenderWindow* window) {
    Ogre::Camera* camera = manager->createCamera(PLAYER_CAMERA);

    camera->setPosition(Ogre::Vector3(0, 160, 160));
    camera->lookAt(Ogre::Vector3(0,0,0));
    camera->setNearClipDistance(5);

    Ogre::Viewport* viewport = window->addViewport(camera);
    viewport->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

    camera->setAspectRatio(Ogre::Real(viewport->getActualWidth()) / Ogre::Real(viewport->getActualHeight()));

    Ogre::SceneNode* cameraNode = create_node(CAMERA_NODE);
    cameraNode->attachObject(camera);

    return camera;
  }

  Ogre::Light* create_light(string name="light.main"){
    manager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

    manager->setAmbientLight(Ogre::ColourValue(0.25, 0.25, 0.25));

    Ogre::Light* light = manager->createLight(name);
    light->setPosition(20, 80, 50);
    light->setCastShadows(true);


    return light;
  }
};


class WindowEventListener: public Ogre::WindowEventListener,
                           public OIS::KeyListener,
                           public OIS::MouseListener {
  OIS::InputManager* inputManager;
  OIS::Keyboard* keyboard;
  OIS::Mouse* mouse;

  typedef vector<OIS::KeyCode> KeyCodes;
  KeyCodes keys_pressed_;
  map<KeyCodes, function<void()>> game_triggers_, menu_triggers_;
public:
  bool exit_;

  WindowEventListener(Ogre::RenderWindow* window) {
    exit_ = false;

    create_input_manager(window);

    keyboard = static_cast<OIS::Keyboard*>(inputManager->createInputObject(OIS::OISKeyboard, true));
    mouse = static_cast<OIS::Mouse*>(inputManager->createInputObject(OIS::OISMouse, true));

    keyboard->setEventCallback(this);
    mouse->setEventCallback(this);
    Ogre::WindowEventUtilities::addWindowEventListener(window, this);
  }

  void capture(void) {
    keyboard->capture();
    mouse->capture();
    Ogre::WindowEventUtilities::messagePump();
  }

  void check_events(void){
    if(menu_triggers_[keys_pressed_]){
      menu_triggers_[keys_pressed_]();
      keys_pressed_.clear();
    }
    else if(game_triggers_[keys_pressed_])
      game_triggers_[keys_pressed_]();
  }

  bool shutdown(const CEGUI::EventArgs &e) {
    exit_ = true;
    cout << ":bye" << endl;
    return true;
  }

  void add_hook(WindowEventListener::KeyCodes keystroke, EventType type, function<void()> callback) {
    if(type == EventType::game_event)
      game_triggers_[keystroke] = callback;
    else{
      menu_triggers_[keystroke] = callback;
    }
    for_each(keystroke.begin(), keystroke.end(),
             [](OIS::KeyCode keycode) {cout << keycode;});
  }

  void remove_key_from_buffer(OIS::KeyCode keycode) {
     auto key = find (keys_pressed_.begin(), keys_pressed_.end(), keycode);
     if(key != keys_pressed_.end())
       keys_pressed_.erase(key);
  }

  bool keyPressed(const OIS::KeyEvent &arg) {
    keys_pressed_.push_back(arg.key);

    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();

    context.injectKeyDown((CEGUI::Key::Scan)arg.key);
    context.injectChar(arg.text);
    return true;
  }

  bool keyReleased(const OIS::KeyEvent& arg) {
    remove_key_from_buffer(arg.key);

    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectKeyUp((CEGUI::Key::Scan)arg.key);

    return true;
  }

  bool mouseMoved(const OIS::MouseEvent&  evt) {
    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
    return true;
  }
  bool mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id) {
    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectMouseButtonDown(convertMouseButton(id));
    return true;
  }
  bool mouseReleased(const OIS::MouseEvent& evt, OIS::MouseButtonID id) {
    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectMouseButtonUp(convertMouseButton(id));
    return true;
  }

private:
  void create_input_manager(Ogre::RenderWindow* window) {
    typedef std::pair<string, string> parameter;
    OIS::ParamList parameters;
    size_t xid = 0;

    window->getCustomAttribute("WINDOW", &xid);
    parameters.insert(parameter("WINDOW", to_string(xid)));
    parameters.insert(parameter("x11_mouse_grab", "false"));
    parameters.insert(parameter("x11_mouse_hide", "false"));
    parameters.insert(parameter("x11_keyboard_grab", "false"));
    parameters.insert(parameter("XAutoRepeatOn", "false"));

    inputManager = OIS::InputManager::createInputSystem(parameters);
  }

  CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id)
  {
    CEGUI::MouseButton ceguiId;
    switch(id)
      {
      case OIS::MB_Left:
        ceguiId = CEGUI::LeftButton;
        break;
      case OIS::MB_Right:
        ceguiId = CEGUI::RightButton;
        break;
      case OIS::MB_Middle:
        ceguiId = CEGUI::MiddleButton;
        break;
      default:
        ceguiId = CEGUI::LeftButton;
      }
    return ceguiId;
  }
};

class GUI{
  Ogre::OverlayManager* overlayManager;

  bool menu_visible;

public:
  CEGUI::Window *menuWindow, *titleWindow;
  Ogre::OverlayElement* overlayElement;

  GUI(){
    CEGUI::OgreRenderer& ogreRenderer =  CEGUI::OgreRenderer::bootstrapSystem();
    menu_visible = false;
    load_resources();

    init_gui();

    create_fps_overlay();
  }

  void switch_menu() {
    menu_visible = !menu_visible;
    menuWindow->setVisible(menu_visible);
  }

  void set_event_callback(CEGUI::Event::Subscriber callback){
    menuWindow->getChild("Exit")->subscribeEvent(
                CEGUI::PushButton::EventClicked,
                callback);
  }

private:
  void load_resources() {
    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
  }

  void init_gui() {
    CEGUI::SchemeManager::getSingleton().createFromFile( "TaharezLook.scheme" );
    CEGUI::GUIContext& guiContext =   CEGUI::System::getSingleton().getDefaultGUIContext();
    guiContext.getMouseCursor().setPosition(CEGUI::Vector2f(1, 0));

    menuWindow = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("TaharezLook.layout");
    guiContext.setRootWindow(menuWindow);
    menuWindow->hide();
  }

  void create_fps_overlay(){
    overlayManager = Ogre::OverlayManager::getSingletonPtr();
    overlayElement = overlayManager->getOverlayElement("fpsInfo");
    Ogre::Overlay *overlay = overlayManager->getByName("Info");
    overlay->show();
  }
};


class Game {
  Ogre::Root* root;

  Ogre::Camera* camera;
  Ogre::Light* light;

  Ogre::OverlayManager* overlayManager;
  Ogre::OverlayElement* overlayElement;

  Ogre::Real last;
  Ogre::Real time_interval;


public:
  GUI* gui;

  const GameConfig config;
  Ogre::RenderWindow* window;

  Ogre::Timer* timer;

  Scene* scene;

  Game()  {
    root = new Ogre::Root(config.plugins, config.ogre, config.ogre_log);

    check_config();

    //WARNING!: If load_resources if called before initialize
    //render system, program will segfault while parsing files.overlay
    window = root->initialise(true, WINDOW_NAME);
    load_resources(config.resource);

    scene = new Scene(root);

    scene->create_ground();
    camera = scene->create_camera(window);
    light = scene->create_light();
    gui = new GUI();

    create_timer();
  }

  ~Game() {
    delete scene;
  }

  Ogre::Real getDeltaTime() {
    Ogre::Real now = timer->getMilliseconds();
    Ogre::Real retval = now - last;

    last = now;

    return retval;
  };

  void render(Ogre::Real deltaT) {
    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();

    time_interval = time_interval + deltaT;
    context.injectTimePulse(time_interval);

    Ogre::Real actual_fps =  window->getAverageFPS();

    if(time_interval >= 1000/TARGET_FPS){
      gui->overlayElement->setCaption(Ogre::StringConverter::toString(actual_fps));
      // config.gameLog->logMessage("FPS: " + to_string(actual_fps));

      root->renderOneFrame();
      time_interval = 0.0;
    }
  }

  void set_camera_mode(Ogre::PolygonMode mode){
    camera->setPolygonMode(mode);
  }

  void show_menu(){
    cout << __func__ << endl;
    gui->switch_menu();
  }

  void start(WindowEventListener& listener) {
    register_keybindings(listener);
    game_loop(listener);
  }


private:
  void check_config(void) {
    if (not (root->restoreConfig() || root->showConfigDialog())) {
      config.systemLog->logMessage("Initialize::configure_ogre => " +
                                   string("ERROR: unable to configure Ogre"));
      throw InvalidConfig();
    }
  }

  void create_timer() {
    timer = root->getTimer();
    timer->reset();
    time_interval = 0.0;
  }

  void load_resources(std::string resources_file) {
    Ogre::ConfigFile cf;
    cf.load(resources_file);

    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    Ogre::String secName, typeName, archName;
    while (seci.hasMoreElements()) {
      secName = seci.peekNextKey();
      Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
      Ogre::ConfigFile::SettingsMultiMap::iterator i;
      for (i = settings->begin(); i != settings->end(); ++i) {
        typeName = i->first;
        archName = i->second;
        Ogre::ResourceGroupManager::getSingleton()
          .addResourceLocation(archName, typeName, secName);
      }
    }

    Ogre::ResourceGroupManager::getSingleton()
      .initialiseAllResourceGroups();
  }

  void register_keybindings(WindowEventListener& listener) {
    cout << __func__ << endl;
    gui->menuWindow->getChild("Exit")->
      subscribeEvent(CEGUI::PushButton::EventClicked,
                     CEGUI::Event::Subscriber(&WindowEventListener::shutdown,
                                              &listener));
    listener.add_hook({OIS::KC_ESCAPE}, EventType::menu_event,
                      bind(&Game::show_menu, this));
  }

  void game_loop(WindowEventListener& listener) {
    Ogre::Real deltaT = 0.000f;
    while (!listener.exit_) {
      listener.capture();
      listener.check_events();
      render(deltaT);
      deltaT = getDeltaTime();
    }
  }
};


int main(int argc, char *argv[])
{
  Game game;
  WindowEventListener listener(game.window);

  game.start(listener);
  return 0;
}
